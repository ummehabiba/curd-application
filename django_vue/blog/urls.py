from django.urls import path
from .views import PostList,PostDetails


urlpatterns=[
    path('posts',PostList.as_view()),  
    path('posts/<int:id>',PostDetails.as_view()), 
]
'''
    path('posts',post_list),
    path('posts/<int:pk>',post_details),
    '''
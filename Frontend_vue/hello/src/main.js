//import Vue from 'vue'
import { createApp } from 'vue'
import App from './App.vue'
import router from './routes'
import 'bootstrap/dist/css/bootstrap.min.css'
//import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap-icons/font/bootstrap-icons.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap'



//Vue.config.productionTip = false

const app = createApp(App).use(router)
app.mount('#app')

/*new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
*/

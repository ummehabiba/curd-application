//import Vue from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import add_tutorial from './components/AddTutorial'
import tutorials_list from './components/TutorialsList'
import tutorial_detail from './components/Tutorial'


//Next you need to call Vue.use(Router) to make sure that Router is added as a middleware to our Vue project.

//Vue.use(VueRouter)

const router = createRouter({
    history: createWebHistory(),
    routes: [
      
      {
      path: "/",
      alias: "/tutorials",
      name: "tutorialList",
      component:tutorials_list,
      //component: () => import("/components/TutorialsList")
      },
      {
        path: "/tutorials/:id",
        name: "tutorial-details",
        component:tutorial_detail,
        //component: () => import("/components/TutorialS")
      },
      {
        path: "/add",
        name: "add",
        component:add_tutorial,
        //component: () => import("/components/AddTutorial")
      }
    ],
  })
export default router
/*
export default new VueRouter({
    //The default mode for Vue Router is hash mode. 
    //It uses a URL hash to simulate a full URL so that the page won’t be reloaded when the URL changes.  
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'BlogPosts',
            component: BlogPosts,
        },
        
    ]
})
*/
import {getAPI} from '../axios-api'

class TutorialDataService {
    getAll() {
      return getAPI.get('/api/list/')
    }
  
    get(id) {
      return getAPI.get(`/api/detail/${id}`)
    }
  
    create(data) {
      return getAPI.post("/api/create/", data)
    }
  
    update(id, data) {
      return getAPI.put(`/api/update/${id}`, data)
      //return getAPI.put(`/api/detail/${id}`, data)
    }
  
    delete(id) {
      return getAPI.delete(`/api/delete/${id}`)
    }
  
    //deleteAll() {
      //return getAPI.delete(`/tutorials`)
    //}
  
    //findByTitle(title) {
     // return http.get(`/tutorials?title=${title}`)
    //}
  }
  
  export default new TutorialDataService()
